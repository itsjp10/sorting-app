package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {

    private String[] input;
    private int[] expectedOutput;

    public SortingAppTest(String[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{"3", "1", "2"}, new int[]{1, 2, 3}},
                // Add more test cases here
        });
    }

    @Test
    public void testSorting() {
        int[] actualOutput = Arrays.stream(input)
                                   .mapToInt(Integer::parseInt)
                                   .toArray();
        Arrays.sort(actualOutput);
        assertArrayEquals(expectedOutput, actualOutput);
    }
}