package com.example;

import java.util.Arrays;

public class SortingApp {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("No arguments provided.");
            return;
        }

        int[] numbers = Arrays.stream(args)
                              .mapToInt(Integer::parseInt)
                              .toArray();
        Arrays.sort(numbers);

        System.out.println("Sorted numbers:");
        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}